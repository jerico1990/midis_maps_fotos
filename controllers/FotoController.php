<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Foto;
use yii\web\UploadedFile;
use app\models\FotoMotivo;

class FotoController extends Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'privado';
        /*
        //image file path
        $imageURL = "http://localhost/midis_maps_fotos/web/fotos/20210111_214309.jpg";

        //get location of image
        $imgLocation = $this->getImageLocation($imageURL);

        //latitude & longitude
        $imgLat = $imgLocation['latitude'];
        $imgLng = $imgLocation['longitude'];
        var_dump($imgLat); die;*/
        return $this->render('index');
    }

    
    public function actionView($archivo){
        $this->layout = 'vacio';
        return $this->render('view',['archivo'=>$archivo]);
    }

    public function actionCreate(){
        
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new Foto();
        $model->titulo_form = 'Agregar galeria de fotos';
        $contador_no_gps = 0;
        $contador_foto = 0 ;
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                
                $model->imagenes = UploadedFile::getInstances($model, 'imagenes');
                foreach ($model->imagenes as $imagen) {
                    if(!$this->getImageLocation($imagen->tempName)){
                        $contador_no_gps++;
                    }
                }
                //var_dump($model->fecha_evento);die;
                $tipos_motivos = explode(",",$model->tipos_motivos);


                if($contador_no_gps>0){
                    return ['success'=>false,'msg'=>'Uno de los archivos no cuenta con GPS'];
                }

                

                foreach ($model->imagenes as $imagen) {
                    $foto = new Foto();
                    $foto->titulo = $model->titulo;
                    $foto->tipos_motivos = $model->tipos_motivos;
                    $imgLocation = $this->getImageLocation($imagen->tempName);
                    $fecha_actual = date ( 'Y-m-d H:i:s'); 
                    $foto->fecha_registro   = $fecha_actual;
                    $foto->estado_registro  = 1;
                    $foto->fecha_evento =date('Y-m-d',strtotime($model->fecha_evento));
                    if($foto->save()){
                        foreach ($tipos_motivos as $tipo_motivo) {
                            $foto_motivo= new FotoMotivo;
                            $foto_motivo->foto_id = $foto->id;
                            $foto_motivo->tipo_motivo_id = $tipo_motivo;
                            $foto_motivo->save();
                        }
                        

                        if($imagen){
                            $imagen->saveAs('fotos/' . $foto->id . '.' . $imagen->extension);
                            $foto->archivo = $foto->id . '.' . $imagen->extension;
                            $foto->descripcion_archivo = $imagen->name;
                        }

                        $latitud = $imgLocation['latitude'];
                        $longitud = $imgLocation['longitude'];
                        $foto->geom    = [$longitud,$latitud];
                        if(!$foto->update()){
                            $contador_foto++;
                        }
                    }else{
                        $contador_foto++;
                    }
                }

                if($contador_foto>0){
                    return ['success'=>false];
                }else{
                    return ['success'=>true];
                }


                


            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }else{
            if ($model->load($request->post())) {
                
                $fecha_actual = date ('Y-m-d H:i:s'); 
                $model->fecha_registro   = $fecha_actual;
                $model->estado_registro  = 1;
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionGetListaFotos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){

            if(isset($_POST['actividades']) && $_POST['actividades']!=''){
                $intArrayActividades = array_map(
                    function($value) { return (int)$value; },
                    $_POST['actividades']
                );
            }

            $features = [];
            $fotosFechasCollection = [];

            $fechas = (new \yii\db\Query())
                ->select(["to_char(fecha_evento, 'YYYY-MM-DD') fecha_evento,titulo"])
                ->from('foto')
                ->innerJoin('foto_motivo','foto_motivo.foto_id=foto.id');
            if(isset($_POST['actividades']) && $_POST['actividades']!=''){
                $fechas = $fechas->andWhere(['in', "foto_motivo.tipo_motivo_id",$intArrayActividades]);
            }

            $fechas = $fechas->groupBy('fecha_evento,titulo')->orderBy('fecha_evento desc')->all();
            
            // if(isset($_POST['actividades']) && $_POST['actividades']!=''){
            //     var_dump($fechas);
            // }

            
            foreach($fechas as $fecha){
                $fotos_fecha = (new \yii\db\Query())
                    ->select('descripcion,descripcion_archivo,archivo,geom,fecha_registro,estado_registro,titulo,fecha_evento')
                    ->from('foto')
                    ->innerJoin('foto_motivo','foto_motivo.foto_id=foto.id');

                if(isset($_POST['actividades']) && $_POST['actividades']!=''){
                    $fotos_fecha = $fotos_fecha->andWhere(['in', "foto_motivo.tipo_motivo_id",$intArrayActividades]);
                }

                $fotos_fecha = $fotos_fecha->andWhere(['=', "estado_registro","1"]);
                $fotos_fecha = $fotos_fecha->andWhere(['=', "to_char(fecha_evento, 'YYYY-MM-DD')",$fecha['fecha_evento']]);
                $fotos_fecha = $fotos_fecha->andWhere(['=', "titulo",$fecha['titulo']]);
                $fotos_fecha = $fotos_fecha->groupBy('descripcion,descripcion_archivo,archivo,geom,fecha_registro,estado_registro,titulo,fecha_evento');
                $fotos_fecha = $fotos_fecha->all();
                array_push($fotosFechasCollection,['fecha_evento'=>$fecha['fecha_evento'],'titulo'=>$fecha['titulo'],'fotos'=>$fotos_fecha]);
            }

            $fotos = (new \yii\db\Query())
                ->select(['descripcion,descripcion_archivo,archivo,geom,fecha_registro,estado_registro,titulo,fecha_evento,ST_AsGeoJSON(geom) as geojson'])
                ->from('foto')
                ->innerJoin('foto_motivo','foto_motivo.foto_id=foto.id');

            if(isset($_POST['actividades']) && $_POST['actividades']!=''){
                $fotos = $fotos->andWhere(['in', "foto_motivo.tipo_motivo_id",$intArrayActividades]);
            }

            $fotos = $fotos->andWhere(['=', "estado_registro","1"]);
            $fotos = $fotos->groupBy('descripcion,descripcion_archivo,archivo,geom,fecha_registro,estado_registro,titulo,fecha_evento');
            $fotos = $fotos->all();

            foreach($fotos as $foto){
                unset($foto['geom']);
                $geometry = $foto['geojson'] = json_decode($foto['geojson']);
                unset($foto['geojson']);
                $feature = ['type'=>'Feature','geometry'=>$geometry,'properties'=>$foto];
                array_push($features,$feature);
            }
            $featureCollection = ['type'=>'FeatureCollection','features'=>$features];

            return ['success'=>true,'featureCollection'=>$featureCollection,'fotosFechasCollection'=>$fotosFechasCollection];
        }
    }



    public function getImageLocation($image = ''){
        $exif = exif_read_data($image, 0, true);
        //var_dump($exif); die;

        if($exif && isset($exif['GPS'])){
            $GPSLatitudeRef = $exif['GPS']['GPSLatitudeRef'];
            $GPSLatitude    = $exif['GPS']['GPSLatitude'];
            $GPSLongitudeRef= $exif['GPS']['GPSLongitudeRef'];
            $GPSLongitude   = $exif['GPS']['GPSLongitude'];
            
            $lat_degrees = count($GPSLatitude) > 0 ? $this->gps2Num($GPSLatitude[0]) : 0;
            $lat_minutes = count($GPSLatitude) > 1 ? $this->gps2Num($GPSLatitude[1]) : 0;
            $lat_seconds = count($GPSLatitude) > 2 ? $this->gps2Num($GPSLatitude[2]) : 0;
            
            $lon_degrees = count($GPSLongitude) > 0 ? $this->gps2Num($GPSLongitude[0]) : 0;
            $lon_minutes = count($GPSLongitude) > 1 ? $this->gps2Num($GPSLongitude[1]) : 0;
            $lon_seconds = count($GPSLongitude) > 2 ? $this->gps2Num($GPSLongitude[2]) : 0;
            
            $lat_direction = ($GPSLatitudeRef == 'W' or $GPSLatitudeRef == 'S') ? -1 : 1;
            $lon_direction = ($GPSLongitudeRef == 'W' or $GPSLongitudeRef == 'S') ? -1 : 1;
            
            $latitude = $lat_direction * ($lat_degrees + ($lat_minutes / 60) + ($lat_seconds / (60*60)));
            $longitude = $lon_direction * ($lon_degrees + ($lon_minutes / 60) + ($lon_seconds / (60*60)));
    
            return array('latitude'=>$latitude, 'longitude'=>$longitude);
        }else{
            return false;
        }
    }

    public function gps2Num($coordPart){
        $parts = explode('/', $coordPart);
        if(count($parts) <= 0)
            return 0;
        if(count($parts) == 1)
            return $parts[0];
        return floatval($parts[0]) / floatval($parts[1]);
    }
}
