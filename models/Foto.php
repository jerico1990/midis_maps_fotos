<?php

namespace app\models;

use Yii;
use nanson\postgis\behaviors\GeometryBehavior;
/**
 * This is the model class for table "foto".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property string|null $descripcion_archivo
 * @property string|null $archivo
 * @property string|null $geom
 * @property string|null $fecha_registro
 * @property int|null $estado_registro
 */
class Foto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo_form;
    public $imagenes;
    public $tipos_motivos;
    public static function tableName()
    {
        return 'foto';
    }

    public function behaviors()
    {
        return [
            [
                'class' => GeometryBehavior::className(),
                'type' => GeometryBehavior::GEOMETRY_POINT,
                'attribute' => 'geom',
                // explicitly set custom db connection if you do not want to use
                // static::getDb() or Yii::$app->getDb() connections
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion','titulo'], 'string'],
            [['fecha_registro'], 'safe'],
            [['estado_registro'], 'default', 'value' => null],
            [['estado_registro'], 'integer'],
            [['descripcion_archivo', 'archivo'], 'string', 'max' => 150],
            [['geom','imagenes','tipos_motivos','fecha_evento'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'descripcion_archivo' => 'Descripcion Archivo',
            'archivo' => 'Archivo',
            'geom' => 'Geom',
            'fecha_registro' => 'Fecha Registro',
            'estado_registro' => 'Estado Registro',
        ];
    }
}
