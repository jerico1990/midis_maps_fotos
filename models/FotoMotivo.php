<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "foto_motivo".
 *
 * @property int $id
 * @property int|null $tipo_motivo_id
 * @property int|null $foto_id
 */
class FotoMotivo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foto_motivo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_motivo_id', 'foto_id'], 'default', 'value' => null],
            [['tipo_motivo_id', 'foto_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_motivo_id' => 'Tipo Motivo ID',
            'foto_id' => 'Foto ID',
        ];
    }
}
