<!-- ================== BEGIN BASE JS ================== -->

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 mt-5">

        <div class="card">
            <?php $form = ActiveForm::begin(); ?>
            <div class="card-header">
                <div class="login-logo">
                    <a href="#"><b>Iniciar sesión</b></a>
                </div>
            </div>
            
            <div class="card-body">
                <div class="row">
                    <div class="mb-3 col-12">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Email" name="LoginForm[username]" id="username">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                    </div>

                    <div class="mb-3 col-12">
                        <div class="form-group has-feedback mb-3">
                            <input type="password" class="form-control" placeholder="Password" name="LoginForm[password]" id="password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Iniciar sesión</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
    
</div>
<!-- /.login-box -->

<script type="text/javascript">
    $('#btn-ingresar').click(function(){

        var error='';
        if($.trim($('#username').val())==''){
            error=error+'Debes ingresar tu usuario <br>';
            $('#username').parent().addClass('has-error');
        }
        else {
            $('#username').parent().removeClass('has-error');
        }

        if($.trim($('#password').val())==''){
            error=error+'Debes ingresar tu contraseña <br>';
            $('#password').parent().addClass('has-error');
        }
        else {
            $('#password').parent().removeClass('has-error');
        }


        if(error!=''){

            return false;
        }else {
            return true;
        }
    });
</script>