<div class="row">
    <div class="col-6">
        <div id="visor" class="visor" style="width:100%;heigth:100hv"></div>
    </div>
    <div class="col-6">
    
    </div>
</div>


<section class="seccion_mapa">
    <div id="visor" class="visor" ></div>
</section>
<section class="seccion_fotos">
    <div class="card col-12">
        <div class="card-header"> <h2>Fotos</h2>  </div>
        <div class="card-body overflow-auto lista-fotos">
            
        </div>
    </div>
</section>


<!-- /.content -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog text-center" role="document">
        <i class="fa fa-refresh fa-spin"></i>
        Cargando
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =  $('#staticBackdrop');
//loading.modal("show");
var map = L.map('visor', {
    zoomControl: false,
    scrollWheelZoom: true,
    fullscreenControl: false,
    dragging: true,
    layers:[],
    minZoom: 3,
    maxNativeZoom: 18,
    maxZoom:18,
    zoom:6,
    center: [-9.1899672, -75.015152],
});


/* Mapa Base */

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//L.esri.basemapLayer('Imagery').addTo(map);

/* Controladores de ubigeo */
var regiones = new L.esri.featureLayer({
	url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
	minZoom: 5,
	style : function (feature) {
		return { color: 'orange', weight: 2 };
	},
});
regiones.addTo(map);



/* Cargando valores a selector*/
regiones.on("load",async function(evt) {

    boundsNacional = L.latLngBounds([]);
    // loop through the features returned by the server
    regiones.eachFeature(async function (layer) {
        // get the bounds of an individual feature
        var layerBounds = layer.getBounds();
        // extend the bounds of the collection to fit the bounds of the new feature
        await boundsNacional.extend(layerBounds);
    });
    await loading.modal('hide');
});


function ScrollView(id) {
    console.log(id);
    /*
    var elmnt = document.getElementById("foto_" + id);
    elmnt.scrollIntoView();*/
}



var geoJsonFotos;
var grupoFotoGeneral = L.layerGroup();
var markersLayer;
async function Listafotos(){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/foto/get-lista-fotos',
        method: 'POST',
        data:{_csrf:csrf},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            grupoFotoGeneral.clearLayers();
            var lista_fotos = "";
            if(results && results.success){
                $.each(results.fotosFechasCollection, function( index, value ) {
                    
                    fecha = moment(value.fecha_registro).format(`dddd, DD MMMM YYYY`);
                    console.log(fecha);

                    lista_fotos = lista_fotos + `<div class="row">
                                                    <h2>${fecha}</h2>
                                                </div>`;
                    row_foto = 1 ;
                    $.each(value.fotos, function( index, value2 ) {
                        if(row_foto==1){
                            lista_fotos = lista_fotos + `<div class="row">`;
                        }
                        lista_fotos = lista_fotos + `<div id="foto_${value2.id}" class="col-3 text-center">
                                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/fotos/${value2.archivo}" class="img-fluid img-thumbnail" alt="...">
                                                    </div>`;
                        if(row_foto==4){
                            lista_fotos = lista_fotos + `</div><br>`;
                            row_foto=0;
                        }
                        row_foto++;
                    });

                    
                });

                $('.lista-fotos').html(lista_fotos)
                
                geoJsonFotos = L.geoJson(results.featureCollection, {
                    onEachFeature: function (feature, layer) {
                        layer.bindPopup(`<img src="<?= \Yii::$app->request->BaseUrl ?>/fotos/${feature.properties.archivo}" class="img-fluid img-thumbnail" alt="...">`);
                        layer.on({
                            click: function (){
                                var elmnt = document.getElementById("foto_" + feature.properties.id);
                                elmnt.scrollIntoView();
                            }
                        });
                    }
                });
                grupoFotoGeneral.addLayer(geoJsonFotos);
                grupoFotoGeneral.addTo(map);

            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}
Listafotos();

// markersLayer.on("click", function (event) {
//     var clickedMarker = event.layer;
//     console.log(clickedMarker);
//     /*
//     var elmnt = document.getElementById("demo");
//     elmnt.scrollIntoView();*/
//     // do some stuff…
// });


/*
    let numFotos = 2;
    $(".lista-fotos .row").slice(numFotos).hide();

    $('.overflow-auto').scroll(() => {
        numFotos = 2;
        if ($('.overflow-auto').scrollTop() + $('.overflow-auto').height() >= $('.card-body').height() - 20) {
            
            $(".lista-fotos .row").slice(numFotos, (numFotos * 2)).fadeIn();
            numFotos = numFotos + numFotos;
            console.log(numFotos);
        }
    });
*/
</script>