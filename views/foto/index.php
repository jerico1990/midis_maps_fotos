<div class="row" style="margin-top:55px">
    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
        <div id="visor" class="visor" ></div>
    </div>
    <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12">
        <div class="lista-fotos overflow-auto">
        </div>
    </div>
    <div class="col-xxl-1 col-xl-1 col-lg-1 col-md-12 col-sm-12 col-xs-12">
    </div>
</div>
<button class="btn btn-primary btn-agregar-foto" type="submit"><i class="fas fa-plus-circle"></i> </button>
<!-- 
<section class="seccion_mapa">
    <div id="visor" class="visor" ></div>
</section>
<section class="seccion_fotos">
    <div class="card col-12">
        <div class="card-header"> <h2>Fotos</h2>  </div>
        <div class="card-body overflow-auto lista-fotos">
            
        </div>
    </div>
</section> -->


<!-- /.content -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog text-center" role="document">
        <i class="fa fa-refresh fa-spin"></i>
        Cargando
    </div>
</div>

<div id="modal2" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>

<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =  $('#staticBackdrop');
//loading.modal("show");
var map = L.map('visor', {
    zoomControl: false,
    scrollWheelZoom: true,
    fullscreenControl: false,
    dragging: true,
    layers:[],
    minZoom: 3,
    maxNativeZoom: 18,
    maxZoom:18,
    zoom:6,
    center: [-9.1899672, -75.015152],
});


/* Mapa Base */

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//L.esri.basemapLayer('Imagery').addTo(map);

/* Controladores de ubigeo */
var regiones = new L.esri.featureLayer({
	url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
	minZoom: 5,
	style : function (feature) {
		return { color: 'orange', weight: 2 };
	},
});
regiones.addTo(map);



/* Cargando valores a selector*/
regiones.on("load",async function(evt) {

    boundsNacional = L.latLngBounds([]);
    // loop through the features returned by the server
    regiones.eachFeature(async function (layer) {
        // get the bounds of an individual feature
        var layerBounds = layer.getBounds();
        // extend the bounds of the collection to fit the bounds of the new feature
        await boundsNacional.extend(layerBounds);
    });
    await loading.modal('hide');
});


function ScrollView(id) {
    console.log(id);
    /*
    var elmnt = document.getElementById("foto_" + id);
    elmnt.scrollIntoView();*/
}



var geoJsonFotos;
var grupoFotoGeneral = L.layerGroup();
var markersCluster = L.markerClusterGroup();
var markersLayer;
async function Listafotos(){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/foto/get-lista-fotos',
        method: 'POST',
        data:{_csrf:csrf,actividades:actividades},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            grupoFotoGeneral.clearLayers();
            markersCluster.clearLayers();
            var lista_fotos = "";
            if(results && results.success){
                $.each(results.fotosFechasCollection, function( index, value ) {
                    
                    fecha = moment(value.fecha_evento).format(`DD MMMM YYYY`);
                    titulo = value.titulo;
                    console.log(value);

                    lista_fotos = lista_fotos + `<div class="row">
                                                    <span> <b> ${titulo} </b>, <small>${fecha}</small></span> 
                                                </div>`;
                    row_foto = 1 ;
                    $.each(value.fotos, function( index, value2 ) {
                        if(row_foto==1){
                            lista_fotos = lista_fotos + `<div class="row">`;
                        }
                        lista_fotos = lista_fotos + `<div id="foto_${value2.id}" class="col-4 col-xxl-4 col-md-4 p-2 text-center">
                                                        <div class="form-group">
                                                            <img src="<?= \Yii::$app->request->BaseUrl ?>/fotos/${value2.archivo}" class="img-fluid btn-abrir-imagen" data-archivo="${value2.archivo}" alt="...">
                                                        </div>
                                                    </div>`;
                        if(row_foto==3){
                            lista_fotos = lista_fotos + `</div><br>`;
                            row_foto=0;
                        }
                        row_foto++;
                    });

                    
                });

                $('.lista-fotos').html(lista_fotos)
                
                geoJsonFotos = L.geoJson(results.featureCollection, {
                    onEachFeature: function (feature, layer) {
                        layer.bindPopup(`<img src="<?= \Yii::$app->request->BaseUrl ?>/fotos/${feature.properties.archivo}" class="img-fluid img-thumbnail" alt="...">`);
                        layer.on({
                            click: function (){
                                var elmnt = document.getElementById("foto_" + feature.properties.id);
                                elmnt.scrollIntoView();
                            }
                        });
                    }
                });
                markersCluster.addLayer(geoJsonFotos);
                map.addLayer(markersCluster);
                /*
                grupoFotoGeneral.addLayer(geoJsonFotos);
                grupoFotoGeneral.addTo(map);*/

            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}
Listafotos();

$('#foto-tipos_motivos').selectpicker();


async function ListaMotivos(){
    $('#foto-tipos_motivos').selectpicker('destroy');
    
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/motivo/get-lista-motivos',
        method: 'POST',
        data:{_csrf:csrf},
        dataType:'Json',
        beforeSend:function()
        {
            //loading.show();
            console.log("1");
        },
        success:function(results)
        {   
            console.log("2");
            //loading.hide();
            if(results && results.success){
                
                var motivos_options ="";
                $.each(results.motivos, function( index, value ) {
                    motivos_options = motivos_options + "<option value='" + value.id + "'>" + value.descripcion + "</option>";
                });
                $('#foto-tipos_motivos').html(motivos_options);
                $('#foto-tipos_motivos').selectpicker();
            }
            
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}


//Seleccionar filtros
var actividades;
$("#foto-actividades").on("changed.bs.select", function(e, clickedIndex, newValue, oldValue) {
    actividades = $(e.currentTarget).val();
    console.log(actividades,"quepaso");
    Listafotos();
});


//abrir

$('body').on('click', '.btn-abrir-imagen', function (e) {
    e.preventDefault();
    archivo = $(this).attr('data-archivo');
    $('#modal2 .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/foto/view?archivo=' + archivo,function(){
        
    });
    $('#modal2').modal('show');
});

//agregar

$('body').on('click', '.btn-agregar-foto', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/foto/create',function(){
        ListaMotivos();
    });
    $('#modal').modal('show');
});

//grabar

$('body').on('click', '.btn-grabar-fotos', function (e) {
    e.preventDefault();
    var form = $('#formFoto');

    // var formData = $('#formFoto').serializeArray();
    // if (form.find('.has-error').length) {
    //     return false;
    // }
    
    var file_length = $('#foto-imagenes').prop("files").length;
    
	var form_data = new FormData();
	form_data.append("_csrf", csrf);

    for (var x = 0; x < file_length; x++) {
        form_data.append("Foto[imagenes][]", document.getElementById('foto-imagenes').files[x]);
    }

	//form_data.append("Foto[imagenes][]", file_data);
    form_data.append("Foto[tipos_motivos]", $('#foto-tipos_motivos').val());
    form_data.append("Foto[titulo]", $('#foto-titulo').val());
    form_data.append("Foto[fecha_evento]", $('#foto-fecha_evento').val());

    $.ajax({
        url:form.attr("action"),
		type: 'POST',
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		dataType:'Json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('No hay conectividad con el sistema');
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                $('#modal').modal('hide');
                document.getElementById("formFoto").reset();
                $("#foto-tipos_motivos").val('default');
                $("#foto-tipos_motivos").selectpicker("refresh");
                Listafotos()
            }else{
                alert(results.msg);
            }
            
        },
    });
});
</script>