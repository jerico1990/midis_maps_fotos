<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Foto */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-content">
    
    <img src="<?= \Yii::$app->request->BaseUrl ?>/fotos/<?= $archivo ?>" class="img-fluid img-thumbnail" alt="">
    
</div>
