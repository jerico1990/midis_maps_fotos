<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Foto */

?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
