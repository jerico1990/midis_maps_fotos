<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Foto */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formFoto','class' => 'form-horizontal']]); ?>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo_form ?></h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="mb-3 col-12">
                <div class="form-group">
                    <label for="">Titulo</label>
                    <input type="text" class="form-control" id="foto-titulo" name="Foto[titulo]">
                </div>
            </div>
            <div class="mb-3 col-12">
                <div class="form-group">
                    <label for="">Actividades</label>
                    <select class=" selectpicker form-control" name="Foto[tipos_motivos]" multiple id="foto-tipos_motivos" title="Seleccionar actividades" data-style="btn-default"></select>
                </div>
            </div>
            <!-- <div class="mb-3 col-12">
                <div class="form-group">
                    <label for="">Descripcion</label>
                    <input type="text" class="form-control" name="Foto[descripcion]" id="foto-descripcion">
                </div>
            </div> -->
            <div class="mb-3 col-12">
                <div class="form-group">
                    <label for="">Motivos</label>
                    <input type="date" class="form-control" id="foto-fecha_evento" name="Foto[fecha_evento]">
                </div>
            </div>
            
            <div class="mb-3 col-12">
                <div class="form-group">
                    <label for="">Imagen</label>
                    <input type="file" class="form-control" multiple name="Foto[imagenes][]" id="foto-imagenes">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary float-end btn-grabar-fotos">Grabar</button>
    </div>
</div>
<?php ActiveForm::end(); ?>


<!-- 

<div class="foto-form row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?php $form = ActiveForm::begin(['options' => ['id' => 'formFoto','class' => 'form-horizontal']]); ?>
        <div class="card">
            <div class="card-header">
                <?= $model->titulo_form ?>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="mb-3 col-12">
                        <div class="form-group">
                            <label for="">Motivos</label>
                            <select class=" selectpicker form-control" name="Foto[tipos_motivos]" multiple id="foto-tipos_motivos" title="Seleccionar motivos" data-style="btn-default"></select>
                        </div>
                    </div>
                    <div class="mb-3 col-12">
                        <div class="form-group">
                            <label for="">Descripcion</label>
                            <input type="text" class="form-control" name="Foto[descripcion]" id="foto-descripcion">
                        </div>
                    </div>
                    <div class="mb-3 col-12">
                        <div class="form-group">
                            <label for="">Imagen</label>
                            <input type="file" class="form-control" multiple name="Foto[imagenes][]" id="foto-imagenes">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary float-end btn-grabar-fotos" >Registrar</button>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div> -->