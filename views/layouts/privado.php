<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MIDIS - Sistema de georeferenciación</title>
    
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <!-- CSS only -->

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS-->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    


    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>

    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script src="https://unpkg.com/esri-leaflet@2.3.3/dist/esri-leaflet.js" integrity="sha512-cMQ5e58BDuu1pr9BQ/eGRn6HaR6Olh0ofcHFWe5XesdCITVuSBiBZZbhCijBe5ya238f/zMMRYIMIIg1jxv4sQ==" crossorigin=""></script>

        <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta/dist/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta/dist/js/bootstrap-select.min.js"></script>
    <script src="https://bowercdn.net/c/es6-promise-3.2.2/es6-promise.min.js"  crossorigin="anonymous"></script>
    <script src="https://bowercdn.net/c/fetch-1.0.0/fetch.js"  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"  crossorigin="anonymous"></script>

    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/leaflet-cluster/MarkerCluster.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/leaflet-cluster/MarkerCluster.Default.css">
    <script src="<?= \Yii::$app->request->BaseUrl ?>/leaflet-cluster/leaflet.markercluster-src.js"  crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    


    <style>
    .bootstrap-select.form-control{
        border:1px solid #ced4da
    }

    .visor{
        height: 90vh;
        width: 100%;
        z-index: 1;
    }
    .lista-fotos{
        height: 90vh;
        width: 100%;
    }
    .lista-fotos .row{
        --bs-gutter-x:0rem;
    }
    .logo{
        height:45px;
        align-content: center;
        align-items: center;
        vertical-align: middle;
    }
    .btn-agregar-foto{
        position: absolute;
        bottom: 20%;
        right: 3%;
        z-index: 10;
    }
    /* .navbar{
        padding-top: 0px;
        padding-bottom: 0px;
    } */

    @media (min-width: 576px) {  }

    @media (min-width: 768px) {
        .lista-fotos img{
            width: 100%;
            height: 100px;
        }  
    }

    @media (min-width: 992px) { 
        .lista-fotos img{
            width: 100%;
            height: 100px;
        }  
    }

    @media (min-width: 1200px) { 
        .lista-fotos img{
            width: 200px;
            height: 250px;
        }  
    }

    @media (min-width: 1400px) { 
        .lista-fotos img{
            width: 200px;
            height: 250px;
        }    
    }

    .leaflet-popup{
        left: -42px;
    }


    .leaflet-popup-content-wrapper{
        height: 120px;
        width: 95px;
        padding:0px;
    }

    .leaflet-popup-content{
        width:95px;
        margin:0px;
    }

    </style>

    
</head>
<body>
    <div class="container-fluid">
        <nav class=" fixed-top navbar-light bg-light">
            <div class="row mt-2">
                <div class="col-1">
                    <img src="http://localhost/midis_maps_fotos/web/img/midis.png" class="logo img-fluid img-thumbnail" alt="">
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <select name="" id="" class="form-control">
                            <option value="">Seleccionar Plataforma</option>
                        </select>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <select class="form-control selectpicker" multiple title="Seleccionar actividades" id="foto-actividades" data-style="btn-default">
                            
                        </select>
                    </div>
                </div>
            </div>

            <!-- <div class="container-fluid">
                <a class="navbar-brand align-items-center">
                    <img src="http://localhost/midis_maps_fotos/web/img/midis.png" class="logo img-fluid img-thumbnail" alt="">
                    
                </a>
                <div class="">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                        <div class="nav-link">
                        <select name="" id="">
                                <option value="">Seleccionar</option>
                            </select>
                        </div>
                            
                        </li>
                        <li class="nav-item">
                            
                        </li>
                        <li class="nav-item">
                            
                        </li>
                    </ul>
                    <div class="d-flex">
                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                Jorge Paredes 
                            </button>
                            <img src="<?= \Yii::$app->request->BaseUrl ?>/img/iniciar_sesion.png" class="logo img-fluid img-thumbnail" alt="">
                            <ul class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                <li><a class="dropdown-item" href="<?= \Yii::$app->request->BaseUrl ?>/login/logout">Cerrar sesión</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->


        </nav>

        <script>
            var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
            ListaActividades();
            async function ListaActividades(){
                $('#foto-actividades').selectpicker('destroy');
                
                await $.ajax({
                    url:'<?= \Yii::$app->request->BaseUrl ?>/motivo/get-lista-motivos',
                    method: 'POST',
                    data:{_csrf:csrf},
                    dataType:'Json',
                    beforeSend:function()
                    {
                        //loading.show();
                        console.log("1");
                    },
                    success:function(results)
                    {   
                        console.log("2");
                        //loading.hide();
                        if(results && results.success){
                            
                            var motivos_options ="";
                            $.each(results.motivos, function( index, value ) {
                                motivos_options = motivos_options + "<option value='" + value.id + "'>" + value.descripcion + "</option>";
                            });
                            $('#foto-actividades').html(motivos_options);
                            $('#foto-actividades').selectpicker();
                        }
                        
                    },
                    error:function(){
                        alert('No hay conectividad con el sistema');
                    }
                });
            }
        </script>
        <?= $content; ?>
        
    </div>
</body>
</html>